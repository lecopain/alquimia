//
//  LoginViewController.m
//  Alquimia
//
//  Created by Fernando Jimenez on 10/24/13.
//  Copyright (c) 2013 Fernando Jimenez. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize signUp = _signUp;
@synthesize userName = _userName;
@synthesize loginButton = _loginButton;
@synthesize password = _password;
@synthesize user = _user;
@synthesize location = _location;
@synthesize statusText = _statusText;
@synthesize fbLoginButton = _fbLoginButton;
@synthesize greyActivityIndicatorView = _activityIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_signUp addTarget:self action:@selector(signUpMethod) forControlEvents:UIControlEventTouchUpInside];
    [_loginButton addTarget:self action:@selector(loginMethod) forControlEvents:UIControlEventTouchUpInside];
    [_fbLoginButton addTarget:self action:@selector(fbLogin)forControlEvents:UIControlEventTouchUpInside];
    
    /*view is not loaded into the view hierarchy
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        [self performSegueWithIdentifier:@"theLogin" sender:self];
    }
    */

}

- (void)viewDidAppear:(BOOL)animated{
    
    //This works at switching view controllers but not sure if it's the best solution
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        //[self performSegueWithIdentifier:@"theLogin" sender:self];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"mapViewController"];
        [vc setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentModalViewController:vc animated:YES];
    }
}


- (void)signUpMethod{
    
    _user = [PFUser user];
    _user.username = [_userName text];
    _user.password = [_password text];
    //user.email = @"email@example.com";
    // other fields can be set just like with PFObject
    //user[@"phone"] = @"415-392-0202";
    
    [_user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            NSLog(@"%@", @"Hooray! Let them use the app now.");
            [_statusText setText:@"You're in :), you can now log in"];
            //create a location Object associated to the user to later save locations
            PFObject *object = [PFObject objectWithClassName:@"Location"];
            object[@"user"] = _user;
            [object setObject:[NSNull null] forKey:@"location"];
            // Save object
            [object saveEventually:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    NSLog(@"Location Saved!!!");
                }
            }];
            
            
            
            
            
        } else {
            NSString *errorString = [error userInfo][@"error"];
            NSLog(@"%@", errorString);
            // Show the errorString somewhere and let the user try again.
        }
    }];
}

#pragma mark - Login mehtods

/* Login to facebook method */
- (void)fbLogin  {
    [[PFFacebookUtils session] handleDidBecomeActive];
    // Set permissions required from the facebook user account
    NSArray *permissionsArray = @[ @"user_about_me", @"user_relationships", @"user_birthday", @"user_location"];
    
    // Login PFUser using facebook
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        [_activityIndicator stopAnimating]; // Hide loading indicator
        
        if (!user) {
            if (!error) {
                NSLog(@"Uh oh. The user cancelled the Facebook login.");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error" message:@"Uh oh. The user cancelled the Facebook login." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
            } else {
                NSLog(@"Uh oh. An error occurred: %@", error);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error" message:[error description] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
            }
        } else if (user.isNew) {
            NSLog(@"User with facebook signed up and logged in!");
            PFObject *object = [PFObject objectWithClassName:@"Location"];
            object[@"user"] = user;
            [object setObject:[NSNull null] forKey:@"location"];
            // Save object
            [object saveEventually:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    NSLog(@"Location Saved!!!");
                    [self performSegueWithIdentifier:@"theLogin" sender:self];

                }
            }];

            
            
        } else {
            NSLog(@"User with facebook logged in!");
             [self performSegueWithIdentifier:@"theLogin" sender:self];
        }
    }];
    
    [_activityIndicator startAnimating]; // Show loading indicator until login is finished
}

- (void) loginMethod{
    
    [PFUser logInWithUsernameInBackground:[_userName text] password:[_password text]
                                    block:^(PFUser *user, NSError *error) {
                                        if (user) {
                                            [self performSegueWithIdentifier:@"theLogin" sender:self];      
                                            
                                        } else {
                                            // The login failed. Check error to see why.
                                        }
                                    }];
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"theLogin"]) {
        // Row selection
        [segue.destinationViewController setLoggedInUser:_user];
    }
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
