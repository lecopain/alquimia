//
//  LoginViewController.h
//  Alquimia
//
//  Created by Fernando Jimenez on 10/24/13.
//  Copyright (c) 2013 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "MapViewController.h"
#import "MapViewController.h"

@interface LoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *signUp;
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet PFUser *user;
@property (weak, nonatomic) IBOutlet PFObject *location;
@property (weak, nonatomic) IBOutlet UIButton *fbLoginButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *greyActivityIndicatorView;

@property (weak, nonatomic) IBOutlet UITextField *statusText;

@end
