//
//  main.m
//  Alquimia
//
//  Created by Fernando Jimenez on 10/18/13.
//  Copyright (c) 2013 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
