//
//  MapViewController.h
//  Alquimia
//
//  Created by Fernando Jimenez on 10/24/13.
//  Copyright (c) 2013 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#include <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>


@interface MapViewController : UIViewController <CLLocationManagerDelegate, MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) PFUser * loggedInUser;
@property (nonatomic, retain) PFObject * locationObject;
@property (nonatomic, retain) NSArray * locations;
@property (nonatomic) BOOL locationSaved;
@property (nonatomic) BOOL friendsDisplayed;



@end
