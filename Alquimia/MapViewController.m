//
//  MapViewController.m
//  Alquimia
//
//  Created by Fernando Jimenez on 10/24/13.
//  Copyright (c) 2013 Fernando Jimenez. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController
@synthesize locationManager = _locationManager;
@synthesize loggedInUser = _loggedInUser;
@synthesize locationObject = _locationObject;
@synthesize locationSaved;
@synthesize friendsDisplayed;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self locationManager] startUpdatingLocation];
    
    //Get location object
    PFQuery *query = [PFQuery queryWithClassName:@"Location"];
    [query whereKey:@"user" equalTo: [PFUser currentUser]];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *location, NSError *error) {
        // comments now contains the comments for myPost
        _locationObject = [location objectAtIndex:0];
    }];
    
    PFQuery *locationQuery = [PFQuery queryWithClassName:@"Location"];
    [locationQuery includeKey:@"user"];
    [locationQuery findObjectsInBackgroundWithBlock:^(NSArray *locations, NSError *error) {
        // comments now contains the comments for myPost
        
        _locations = locations;
        
        
        for (PFObject * friendLocation in _locations) {
            
            NSString *friendName = [[friendLocation objectForKey:@"user"] username];
            
            if([friendName isEqualToString:[[PFUser currentUser] username]]){
                continue;
            }
            
            MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
            PFGeoPoint * friendPoint = (PFGeoPoint *)[friendLocation objectForKey:@"location"];
            
            if((NSNull *)friendPoint != [NSNull null]){
            point.coordinate = CLLocationCoordinate2DMake(friendPoint.latitude, friendPoint.longitude);
            point.title = [[friendLocation objectForKey:@"user"] username];
            [self.mapView addAnnotation:point];
            }
        }

    }];
}

#pragma mark - MKMapViewDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    static NSString *GeoPointAnnotationIdentifier = @"RedPin";
    
    MKPinAnnotationView *annotationView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:GeoPointAnnotationIdentifier];
    
    if (!annotationView) {
        annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:GeoPointAnnotationIdentifier];
        annotationView.pinColor = MKPinAnnotationColorRed;
        annotationView.canShowCallout = YES;
        annotationView.draggable = YES;
        annotationView.animatesDrop = YES;
    }
    
    return annotationView;
}


- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    
    
    if(_locationObject != NULL && !locationSaved){
        
        //NSLog(@"latitude: %f, longitude: %f", coordinate.latitude, coordinate. longitude);
        CLLocation *location = _locationManager.location;
        CLLocationCoordinate2D coordinate = [location coordinate];
        PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLatitude:coordinate.latitude
                                                      longitude:coordinate.longitude];
        
        
        _locationObject[@"location"] = geoPoint;
        [_locationObject saveInBackground];
        
        //centering map at users location
         self.mapView.region = MKCoordinateRegionMake(CLLocationCoordinate2DMake(geoPoint.latitude, geoPoint.longitude), MKCoordinateSpanMake(0.01f, 0.01f));
        
        //user has been saved, no need to use this method again
        locationSaved = YES;


    }

}



- (CLLocationManager *)locationManager {
    if (_locationManager != nil) {
        return _locationManager;
    }
    
    _locationManager = [[CLLocationManager alloc] init];
    [_locationManager setDesiredAccuracy:kCLLocationAccuracyNearestTenMeters];
    [_locationManager setDelegate:self];
    [_locationManager setPurpose:@"Your current location is used to demonstrate PFGeoPoint and Geo Queries."];
    
    return _locationManager;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
