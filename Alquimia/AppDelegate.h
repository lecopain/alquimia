//
//  AppDelegate.h
//  Alquimia
//
//  Created by Fernando Jimenez on 10/18/13.
//  Copyright (c) 2013 Fernando Jimenez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
